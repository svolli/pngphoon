
#include "moon.h"
#include "moondata.h"
#include "tws.h"
#include "phase.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

static const unsigned char  leftmask[8] = { 0xff, 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01 };
static const unsigned char rightmask[8] = { 0x00, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe };

static const unsigned char shades[16][8] =
{
   { 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
   { 0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
   { 0x7f,0xff,0xdf,0xff,0xff,0xff,0xff,0xff },
   { 0x7f,0xff,0xdf,0xff,0xfe,0xff,0xff,0xff },
   { 0x7f,0xff,0xdf,0xff,0xfe,0xff,0xff,0xf7 },
   { 0x7f,0xfd,0xdf,0xff,0xfe,0xff,0xff,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xfe,0xff,0xff,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xfe,0xff,0x7f,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xfe,0xfb,0x7f,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xbe,0xfb,0x7f,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xbe,0xfb,0x5f,0xf7 },
   { 0x7f,0xfd,0xdf,0xfb,0xbe,0xfb,0x5f,0xf5 },
   { 0x7f,0xfd,0x5f,0xfb,0xbe,0xfb,0x5f,0xf5 },
   { 0x7f,0xf5,0x5f,0xfb,0xbe,0xfb,0x5f,0xf5 },
   { 0x7f,0xf5,0x5f,0xfb,0xae,0xfb,0x5f,0xf5 },
   { 0x5f,0xf5,0x5f,0xfb,0xae,0xfb,0x5f,0xf5 }
};


#define PI 3.14159265358979323846  /* assume not near black hole or in
                                      Tennessee */

moon_t *mooncreate()
{
   moon_t *moondata;

   moondata = (moon_t*)malloc(sizeof( moon_t ));
   moondata->bitmap = (png_bytep)moon_bits;
   moondata->width  = moon_width;
   moondata->height = moon_height;
   moondata->xbytes = (moon_width + 7) / 8;

   return moondata;
}


void moondestroy( moon_t *moon )
{
   free( moon );
   moon = NULL;
}


void mooncopy(image_t *image, moon_t *moondata, int cx, int cy, int blackflag)
{
   double jd = 0.0;
   double angphase = 0.0;
   double cphase = 0.0;
   double aom = 0.0;
   double cdist = 0.0;
   double cangdia = 0.0;
   double csund = 0.0;
   double csuang = 0.0;
   struct tws* t = 0;
   int r = moondata->width/2;
   int i = 0;
   register int x = 0;
   register int y = 0;
   int xleft = 0;
   int xright = 0;
   double fxleft = 0.0;
   double fxright = 0.0;
   double fy = 0.0;
   int bytexleft = 0;
   int bitxleft = 0;
   int bytexright = 0;
   int bitxright = 0;
   int moonoff = 0;
   int imageoff = 0;
   double cap = 0.0;
   double ratio = 0.0;
   int shadeindex = 0;
   unsigned char shade = 0;
   int xoffset = 0;
   int yoffset = 0;

   t = dtwstime();

   xoffset = (cx - moondata->width/2);
   yoffset = (cy - moondata->height/2);

   jd = jtime( t );

   angphase = phase( jd, &cphase, &aom, &cdist, &cangdia, &csund, &csuang );
   cap = cos( angphase );

   /* Hack to figure approximate earthlighting. */
   if ( cphase < 0.1 ) cphase = 0.1;
   if ( cphase > 0.9 ) cphase = 0.9;
   ratio = (1.0 - cphase) / cphase;        /* ratio varies from 9.0 to 0.111 */
   shadeindex = (int) ( ratio / 9.0 * 15.9999 );

   for ( i = 0; i < 2 * r; ++i )
   {
      y = moondata->height/2 - r + i;
      if ( y < 0 || y >= (int)moondata->height )
      {
         continue;
      }
      fy = i - r;
      fxright = r * sqrt( 1.0 - ( fy * fy ) / ( r * r ) );
      fxleft = - fxright;

      if ( angphase >= 0.0 && angphase < M_PI )
      {
         fxright *= cap;
      }
      else
      {
         fxleft *= cap;
      }

      xleft = fxleft + moondata->width/2 + 0.5;
      xright = fxright + moondata->width/2 + 0.5;

      bytexleft = xleft / 8;
      bitxleft = xleft % 8;

      bytexright = xright / 8;
      bitxright = xright % 8;

      moonoff  = y * ( moondata->width ) / 8;
      imageoff = (y + yoffset) * image->xbytes + xoffset / 8;

      shade = blackflag ? shade = 0xff : shades[shadeindex][y % 8];

      /* first copy the stuff */
      for( x = 0; x < (int)moondata->width/8; ++x )
      {
         if( moondata->bitmap[x + moonoff] )
         {
            image->bitmap[x + imageoff] = moondata->bitmap[x + moonoff];
         }
      }

      /* now, hack the bits */
      if( bytexleft == bytexright )
      {
         image->bitmap[bytexleft + imageoff] &=
               ~(leftmask[bitxleft] & shade & rightmask[bitxright]);
      }
      else
      {
         image->bitmap[bytexleft + imageoff] &= ~(leftmask[bitxleft] & shade);
         for( x = bytexleft + 1; x < bytexright; ++x )
         {
            image->bitmap[x + imageoff] &= ~shade;
         }
         image->bitmap[bytexright + imageoff] &= ~(rightmask[bitxright] & shade);
      }
   }
}
