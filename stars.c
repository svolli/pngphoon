
#include "stars.h"
#include <stdlib.h>

#define NUM_STARS 9

#define NUM_TILES 20
#define TILE_SIZE 128
#define STARS_PER_TILE 40

struct star_s
{
   int width;
   int height;
   unsigned char data[5];
};

typedef struct star_s star_t;

star_t stars[NUM_STARS] =
{
   {1, 1, { 0x80, 0x00, 0x00, 0x00, 0x00 }},
   {2, 2, { 0xc0, 0xc0, 0x00, 0x00, 0x00 }},
   {3, 3, { 0x40, 0xe0, 0x40, 0x00, 0x00 }},
   {3, 3, { 0x40, 0xa0, 0x40, 0x00, 0x00 }},
   {4, 4, { 0x60, 0xf0, 0xf0, 0x60, 0x00 }},
   {4, 4, { 0x20, 0xe0, 0x70, 0x40, 0x00 }},
   {4, 4, { 0x40, 0x70, 0xe0, 0x20, 0x00 }},
   {5, 5, { 0x70, 0xf8, 0xf8, 0xf8, 0x70 }},
   {5, 5, { 0x20, 0x70, 0xf8, 0x70, 0x20 }}
};

void stardraw( image_t *image, star_t *star, int xpos, int ypos )
{
   int y;
   png_bytep p;

   union
   {
      unsigned short u16;
      unsigned char  u8[2];
   } shift;

   for( y = 0; y < star->height; y++ )
   {
      p = image->bitmap + (xpos / 8) + ( (ypos + y) * image->xbytes );
      shift.u16 =  star->data[y] << 8;
      shift.u16 >>= (xpos % 8);
      *p |= shift.u8[1];
      if( shift.u8[0] )
      {
         p++;
         *p |= shift.u8[0];
      }
   }
}

void scarymonster( image_t *image, int density )
{
   /*
    * why is this procedure called scarymonster when it draws a starry sky?
    * There once was a song in the beginning of the 1980's from Hubert Kah
    * that's called "Sternenhimmel" (starry sky). There was also an english
    * language version available by the same artist that was called...
    * you guessed it: "Scary Monster".
    *
    * Maybe this one here will get me listed at the daily wtf. :-)
    */
   int i, k, r, x, y;
   int total_prob;
   int num_stars = (image->width * image->height * density) / 50000 ;
   const int star_prob[NUM_STARS] = { 700, 60, 15, 15,  6,  6,  6,  2,  2 };
   /*                                   1   2  3a  3b  4a  4b  4c  5a  5b */


   /* seed the pseudo random generator
    * this way the stars will always be in the same place which is
    * quite nice during reloads. */
   srand( image->width * image->height );

   total_prob = 0;
   for ( i = 0; i < NUM_STARS; ++i )
   {
      total_prob += star_prob[i];
   }

   for( i = 0; i < num_stars; i++ )
   {
      r = rand() % total_prob;
      for ( k = 0; k < NUM_STARS; ++k )
      {
         r -= star_prob[k];
         if ( r < 0 )
         {
            x = rand() % ( image->width  - stars[k].width );
            y = rand() % ( image->height - stars[k].height );
            stardraw( image, &(stars[k]), x ,y );
            break;
         }
      }
   }
}
