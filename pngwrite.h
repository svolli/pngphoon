
#ifndef _PNGWRITE_H_
#define _PNGWRITE_H_ _PNGWRITE_H_

#include "image.h"

void pngwrite( image_t *image, char* filename, int height, png_colorp palette_p );

#endif
