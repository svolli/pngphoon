
#ifndef _FAIL_H_
#define _FAIL_H_ _FAIL_H_

void fail( const char * s, ... );
void warn( const char * s, ... );
void disablewarn();

#endif /* _FAIL_H_ */
