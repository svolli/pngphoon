
CC ?= gcc
STRIP ?= strip
UPX ?= upx
CFLAGS += -Os -Wall -Wextra -pedantic -DSYS5=1
LDFLAGS += -lpng -lz -lm
EXT ?=

all: pngphoon$(EXT)

.PHONY: man pack clean

pngphoon$(EXT): image.o pngwrite.o moon.o main.o phase.o tws.o stars.o fail.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

contrib/pngphoon.1: contrib/pngphoon.sgml
	docbook-to-man $< > $@

man: contrib/pngphoon.1

pack: pngphoon$(EXT)
	$(STRIP) -R .note -R .comment $<
	$(UPX) --ultra-brute $<

clean:
	rm -f *.o pngphoon$(EXT)

